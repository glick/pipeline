# standard

# external
import dotty_dict
import pytest
import semver

# local


# module globals and constants


def is_valid(version):
    """return True if version is valid else False"""
    try:
        semver.parse(version)
        return True
    except ValueError as err:
        return False


@pytest.mark.parametrize(
    "version,expected", [("2.0.4", True), ("2.00.04", False)]
)
def test_is_valid(version, expected):
    assert is_valid(version) is expected


@pytest.mark.parametrize(
    "version,rule,expected", [(dotty_dict.__version__, ">=1.1.0", True)]
)
def test_version_rule(version, rule, expected):
    assert semver.match(version, rule) is expected
