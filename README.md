# Pipeline

sample of GitLab CI pipeline

[![GitPitch](https://gitpitch.com/assets/badge.svg)](https://gitpitch.com/glick/pipeline/master?grs=gitlab)

# Développer avec GitLab et git
```mermaid
graph TD
demande["demande (issue)"] -->  assignation
assignation --> branche
branche --> MR[Merge Request]
branche --> dev["dev en TDD (commit/push)"]
dev -- déclenche --> CI[Intégration Continue]
CI --> MR
MR --> maintainer{Mainteneur}
maintainer -- accepte --> merge["Fusion (merge)"]
maintainer -- refuse --> dev
merge -- ferme --> MR
merge -- supprime --> branche
merge -- ferme --> demande
merge -.->|déclenche| CI

classDef classProjet fill:#f9f,stroke:#333;
class demande,assignation,branche,MR,dev,CI,maintainer,merge classProjet;
```
