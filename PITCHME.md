# CI/CD pipeline

Gaël Lickindorf - Consultant DevOps

+++

## Introduction

Présentation en ligne

https://gitlab.com/glick/pipeline  
https://gitpitch.com/glick/pipeline/master?grs=gitlab

---
## 1. CI/CD pipeline

<img src="img/CI_CD_pipeline.png"/>

+++

<img src="img/Detailed_pipeline.png" />

---
## 2. pre-commit

https://pre-commit.com/

Automate Python workflow using pre-commits: black and flake8:   
https://ljvmiranda921.github.io/notebook/2018/06/21/precommits-using-black-and-flake8/
